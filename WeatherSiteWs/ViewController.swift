//  ViewController.swift
//  WeatherSiteWs
//  Created by Dmitry Alexandrov
//  Read it:
//  https://ioscoachfrank.com/remove-main-storyboard.html
import UIKit

class ViewController: UIViewController, UITextFieldDelegate
{
    var topView: UIView?
    var topLabel: UILabel?
    var inputCity: UITextField?
    var getPredictButton: UIButton?
    var predictTextView: UITextView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addView()
        inputCity!.delegate = self
    }
    
    
    private func setupView() {
        topLabel = {
            let control = UILabel()
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.lightGray
            control.text = "Weather forecast"
            control.translatesAutoresizingMaskIntoConstraints = false // required
            return control
        }()
        
        inputCity = {
            let control = UITextField()
            control.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.black
            control.backgroundColor = UIColor.white
            control.placeholder = "Enter city"
            control.textAlignment = .center
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            return control
        }()
        
        getPredictButton = {
            let control = UIButton()
            control.backgroundColor = UIColor.gray
            control.layer.cornerRadius = 8
            control.clipsToBounds = false
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.setTitle("Get forecast", for: .normal)
            control.addTarget(self, action: #selector(getForecast), for: .touchUpInside)
            control.tag = 1
            return control
        }()
        
        predictTextView = {
            let control = UITextView()
            control.backgroundColor = UIColor.lightGray
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.white
            control.text = "Enter city and tap 'Get forecast'."
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            control.isEditable = false
            return control
        }()
    }
    
    func addView() {
        view.addSubview(topLabel!)
        view.addSubview(getPredictButton!)
        view.addSubview(inputCity!)
        view.addSubview(predictTextView!)
        
        topLabel!.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive =  true
        topLabel!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =  true
        
        inputCity!.topAnchor.constraint(equalTo: topLabel!.bottomAnchor, constant: 20).isActive = true
        inputCity!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive =  true
        inputCity!.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        inputCity!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        getPredictButton!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        getPredictButton!.topAnchor.constraint(equalTo: inputCity!.bottomAnchor, constant: 20).isActive = true
        getPredictButton!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        getPredictButton!.widthAnchor.constraint(equalToConstant: 180).isActive = true
        
        predictTextView!.topAnchor.constraint(equalTo: getPredictButton!.bottomAnchor, constant: 20).isActive = true
        predictTextView!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        
        predictTextView!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        predictTextView!.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
    }
    
    
    @objc func getForecast(_ sender: UIButton) {
        var s = inputCity!.text!.trimmingTrailingSpaces()
        s = s.trimmingLeadingSpaces()
        inputCity!.text = s
        if s == "" {
            predictTextView!.text = "Enter city and tap 'Get forecast'."
            return
        }
        var weather = ""
        let url = URL(string: "http://www.weather-forecast.com/locations/\(s.replacingOccurrences(of: " ", with: "-"))/forecasts/latest")
        var urlError = false
        
        if url != nil {
            let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error == nil {
                    let urlContent = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    let urlContentArray = urlContent?.components(separatedBy: "<span class=\"phrase\">")
                    
                    if (urlContentArray?.count)! > 1 {
                        let weatherArray = urlContentArray!.last!.components(separatedBy: "</span>")
                        weather = (weatherArray.first)!
                        weather = weather.replacingOccurrences(of: "&deg", with: "°")
                    } else {
                        urlError = true
                    }
                } else {
                    urlError = true
                }
                
                DispatchQueue.main.async {
                    if urlError == true {
                        self.showError(s)
                    } else {
                        self.predictTextView!.text = weather
                    }
                }
            }
            task.resume()
        } else {
            showError(s)
        }
    }
    
    
    func showError(_ s: String) {
        predictTextView!.text = "Was not able to find weather forecast for \(s). Please try again."
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputCity!.resignFirstResponder()
        getForecast(getPredictButton!)
        return true
    }
}


extension String {
    func trimmingTrailingSpaces() -> String {
        var s = self
        while s.hasSuffix(" ") {
            s = "" + s.dropLast()
        }
        return s
    }
    
    func trimmingLeadingSpaces() -> String {
        var s = self
        while s.hasPrefix(" ") {
            s = s.dropFirst() + ""
        }
        return s
    }
}
